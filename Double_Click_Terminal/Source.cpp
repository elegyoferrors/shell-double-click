//Copyright Chris DeVisser - Elegy of Errors 2013
//Code may be used freely in personal and commercial environments.
//Compiled with MSVC12.
//Shell interface code attributed to http://blogs.msdn.com/b/oldnewthing/archive/2004/07/20/188696.aspx

//Windows XP required
#define _WIN32_WINNT 0x0501
#define WINVER 0x0501
#define NTDDR_VERSION 0x0501
#define UNICODE

#include <algorithm>
#include <array>
#include <chrono>
#include <iostream>

#include <windows.h>
#include <exdisp.h>
#include <shlobj.h>
#include <shobjidl.h>
#include <shlwapi.h>

//Get an IWebBrowserApp * with passed in HWND if one exists.
auto webBrowserAppFromHwnd(HWND hwnd, IWebBrowserApp *&out) noexcept{
	HRESULT hr;

	IShellWindows *sw{};
	hr = CoCreateInstance(CLSID_ShellWindows, nullptr, CLSCTX_ALL, IID_IShellWindows, reinterpret_cast<void **>(&sw));

	VARIANT v;
	VariantInit(&v);
	V_VT(&v) = VT_I4;

	out = nullptr;

	long count;
	sw->get_Count(&count);

	IDispatch *dis;
	IWebBrowserApp *wba{};
	for (V_I4(&v) = 0; V_I4(&v) < count && sw->Item(v, &dis) == S_OK; ++V_I4(&v)) {
		hr = dis->QueryInterface(IID_IWebBrowserApp, reinterpret_cast<void **>(&wba));

		HWND wbaHwnd;
		wba->get_HWND(reinterpret_cast<LONG_PTR *>(&wbaHwnd));

		if (wbaHwnd == GetAncestor(hwnd, GA_ROOT)) {
			out = wba;
			break;
		}

		wba->Release();
	}

	dis->Release();
	if (sw) { sw->Release(); }
	VariantClear(&v);
}

//Gets an IShellBrowser from an IWebBrowserApp.
auto shellBrowserFromWebBrowserApp(IWebBrowserApp *wba) noexcept {
	IServiceProvider *sp;
	wba->QueryInterface(IID_IServiceProvider, reinterpret_cast<void **>(&sp));

	IShellBrowser *sb;
	sp->QueryService(SID_STopLevelBrowser, IID_IShellBrowser, reinterpret_cast<void **>(&sb));

	sp->Release();
	wba->Release();
	return sb;
}

//Gets the active shell view from an IShellBrowser
auto shellViewFromShellBrowser(IShellBrowser *sb) noexcept {
	IShellView *sv;
	sb->QueryActiveShellView(&sv);
	sb->Release();
	return sv;
}

//Gets the folder view from an IShellView
auto folderViewFromShellView(IShellView *sv) noexcept {
	IFolderView *fv;
	sv->QueryInterface(IID_IFolderView, reinterpret_cast<void **>(&fv));
	sv->Release();
	return fv;
}

auto desktopShellView() noexcept {
	IShellFolder *sf;
	SHGetDesktopFolder(&sf);

	IShellView *sv;
	sf->CreateViewObject(nullptr, IID_IShellView, reinterpret_cast<void **>(&sv));

	return sv;
}

bool isDesktop(HWND hwnd) {
	return GetAncestor(hwnd, GA_ROOT) == GetShellWindow();
}

//Is the window handle to a window owned by explorer.exe?
IFolderView *getExplorerWindowFolderView(const HWND hwnd) noexcept{
	IShellView *sv{};

	if (isDesktop(hwnd)) {
		sv = desktopShellView();
	}
	else {
		IWebBrowserApp *wba;
		webBrowserAppFromHwnd(hwnd, wba);
		if (!wba) { return nullptr; }

		auto sb = shellBrowserFromWebBrowserApp(wba);
		if (!sb) { return nullptr; }

		sv = shellViewFromShellBrowser(sb);
	}

	if (!sv) { return nullptr; }
	return folderViewFromShellView(sv);
}

std::wstring getFolderPath(IFolderView *fv) noexcept {
	IPersistFolder2 *pf;
	fv->GetFolder(IID_IPersistFolder2, reinterpret_cast<void **>(&pf));

	LPITEMIDLIST pidl;
	pf->GetCurFolder(&pidl);

	std::wstring path(MAX_PATH, '\0');
	SHGetPathFromIDList(pidl, &path[0]);

	CoTaskMemFree(pidl);
	pf->Release();

	return path.c_str();
}

bool isObjectFocused(IFolderView *fv, HWND hwnd) {
	if (isDesktop(hwnd)) { return SendMessage(hwnd, LVM_GETSELECTEDCOUNT, 0, 0) != 0; }

	int count;
	fv->ItemCount(SVGIO_SELECTION, &count);
	return count != 0;
}

void spawnCommandPrompt(const std::wstring &path) noexcept{
	const std::wstring args = L"/K \"cd /d " + path + L"\"";
	ShellExecute(nullptr, L"open", L"C:\\Windows\\system32\\cmd.exe", nullptr, path.c_str(), SW_SHOW);
}

//Is double click within the rectangle from the first click?
auto withinRect(const POINT &oldPos, const POINT &newPos) noexcept{
	const auto MAX_WIDTH = GetSystemMetrics(SM_CXDOUBLECLK);
	const auto MAX_HEIGHT = GetSystemMetrics(SM_CYDOUBLECLK);

	return
		newPos.x < oldPos.x + MAX_WIDTH / 2 &&
		newPos.x > oldPos.x - MAX_WIDTH / 2 &&
		newPos.y < oldPos.y + MAX_HEIGHT / 2 &&
		newPos.y > oldPos.y - MAX_HEIGHT / 2;
}

//Catch double clicks and spawn command prompt if explorer window.
auto CALLBACK mouseProc(const int code, const WPARAM wParam, const LPARAM lParam) noexcept{
	using clock = std::chrono::steady_clock;
	const auto MAX_DBLCLK_MS = std::chrono::milliseconds(GetDoubleClickTime());

	const auto ret = CallNextHookEx(nullptr, code, wParam, lParam);
	if (code < 0 || wParam != WM_LBUTTONUP) { return ret; }

	static bool onSecondClick;
	static clock::time_point clickTime;
	static POINT clickPoint;
	static HWND clickWindow;

	if (!onSecondClick) {
		onSecondClick = true;
		clickTime = clock::now();
		GetCursorPos(&clickPoint);
		clickWindow = WindowFromPoint(clickPoint);
		return ret;
	}

	POINT newPoint;
	GetCursorPos(&newPoint);

	auto newTime = clock::now();
	auto newWindow = WindowFromPoint(newPoint);

	std::swap(clickTime, newTime);
	std::swap(clickPoint, newPoint);
	std::swap(clickWindow, newWindow);

	if (clickWindow != newWindow) { return ret; }
	if (clickTime - newTime > MAX_DBLCLK_MS || !withinRect(newPoint, clickPoint)) {
		return ret;
	}

	auto fv = getExplorerWindowFolderView(clickWindow);
	if (fv) {
		if (!isObjectFocused(fv, clickWindow)) {
			spawnCommandPrompt(getFolderPath(fv));
		}

		fv->Release();
	}

	return ret;
}

int main() noexcept{
	CoInitializeEx(nullptr, 0);

	auto hook = SetWindowsHookEx(WH_MOUSE_LL, mouseProc, GetModuleHandle(nullptr), 0);

	MSG msg;
	while (GetMessage(&msg, nullptr, 0, 0) > 0) {
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	UnhookWindowsHookEx(hook);
	CoUninitialize();
}
